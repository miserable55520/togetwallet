import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import _ from 'lodash';
import VueMaterial from 'vue-material'
import VueClipboard from 'vue-clipboard2'

Vue.use(VueClipboard);
Vue.set(Vue.prototype, '_', _);

Vue.config.productionTip = false;
Vue.config.keyCodes.backspace = 8;

Vue.use(VueMaterial);
new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app');
